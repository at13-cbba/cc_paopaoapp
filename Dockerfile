FROM openjdk:11
EXPOSE 8080
WORKDIR /app
COPY ./build/libs/*.jar /app/converter-0.0.1-SNAPSHOT.jar
COPY ./archive /app/archive
ENTRYPOINT [ "java", "-jar", "converter-0.0.1-SNAPSHOT.jar" ]